// Add all functions which return html into this directory

import './view.scss'

import { PokemonService} from "@/service/service";
import { createHeader } from "./header/header";
import { createPokemonDeck, loadPokemonsDown, loadPokemonsUp } from "./main/deck/deck";

//функция отрисовки всего
export async function draw():Promise<void>{

    const pokemonService:PokemonService = PokemonService.getInstance();
    await pokemonService.firstLoadPokemons();

    const root:HTMLElement = document.getElementById('root');

    root.appendChild(createHeader());

    const pokemonDeck:HTMLElement = await createPokemonDeck()
    root.appendChild(pokemonDeck);

    document.addEventListener('scroll', async (e:Event):Promise<void>=>{
        //определение координат (сделано та, что этот скроллхендлер можно перевесить как на документ, так и на отдельный элемент)
        const target:Document | HTMLElement = e.target as Document | HTMLElement;
        const scrollPosition:number = target instanceof Document ? target.documentElement.scrollTop || target.body.scrollTop : target.scrollTop;
        const scrollHeight:number = target instanceof Document ? target.documentElement.scrollHeight || target.body.scrollHeight : target.scrollHeight;
        const clientHeight:number = target instanceof Document ? target.documentElement.clientHeight : target.clientHeight;
    
        //если еще не подгружается, то начнётся загрузка новых покемонов на страниццу
        if(!pokemonService.isFetching){
            pokemonService.isFetching = true;
            if(scrollHeight - (scrollPosition + clientHeight) < 300){
                await loadPokemonsDown(pokemonDeck);
            }
            else if (scrollPosition < 400){
                await loadPokemonsUp(pokemonDeck);
            }
            pokemonService.isFetching = false;
        }
    });
}

