import styles from './header.module.scss'

//создание шапки страницы
export function createHeader():HTMLElement{
    const header:HTMLElement = document.createElement('header');
    header.classList.add(styles.header);
    const h1:HTMLElement = document.createElement('h1');
    h1.classList.add(styles.header__title);
    h1.innerHTML = `Pokedex <span>catch the pokemons!</span>`
    header.appendChild(h1);
    return header;
}