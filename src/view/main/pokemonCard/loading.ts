import styles from './card.module.scss'

//создать карточку загрузки
export function createLoading():HTMLLIElement{
    const loadingLI:HTMLLIElement = document.createElement('li');
    loadingLI.classList.add(styles.liContainer);

    const loadingCard:HTMLDivElement = document.createElement('div');
    loadingCard.classList.add(styles.pokemon);
    loadingCard.classList.add(styles.loading);

    loadingLI.append(loadingCard);
    return loadingLI;
}