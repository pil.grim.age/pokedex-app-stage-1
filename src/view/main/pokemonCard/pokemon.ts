import styles from './card.module.scss'

import { IPokemon } from "@/model/model";
import { catchPokemon } from "@/service/service";

//создать карточку одного покемона
export function createPokemon(pokemon : IPokemon):HTMLLIElement{
    const pokemonLI:HTMLLIElement = document.createElement('li');
    pokemonLI.classList.add(styles.liContainer);

    const pokemonCard:HTMLDivElement = document.createElement('div');
    pokemonCard.id = `pokemon-${String(pokemon.id)}`;
    pokemonCard.classList.add(styles.pokemon);

    const pokemonImgContainer:HTMLDivElement = document.createElement('div');
    pokemonImgContainer.classList.add(styles.pokemon__imgContainer);
    const pokemonImg:HTMLImageElement = document.createElement('img');
    pokemonImg.classList.add(styles.pokemon__img);
    pokemonImg.src = pokemon.imgUrl;
    pokemonImg.alt = `image of ${pokemon.name}`;
    pokemonImgContainer.appendChild(pokemonImg);
    pokemonCard.appendChild(pokemonImgContainer);

    const pokemonName:HTMLDivElement = document.createElement('div');
    pokemonName.classList.add(styles.pokemon__name);
    pokemonName.innerHTML = pokemon.name;
    pokemonCard.appendChild(pokemonName); 

    const pokemonID:HTMLDivElement = document.createElement('div');
    pokemonID.classList.add(styles.pokemon__id);
    pokemonID.innerHTML = '#' + String(pokemon.id);
    pokemonCard.appendChild(pokemonID);
    
    const pokemonButt:HTMLButtonElement = document.createElement('button');
    pokemonButt.classList.add(styles.pokemon__button);
    if(pokemon.isCatched){
        pokemonButt.innerHTML = "Caught";
        pokemonButt.classList.add(styles.caught)
    }
    else{
        pokemonButt.innerHTML = 'Catch!'
        pokemonButt.addEventListener('click', ()=>{
            pokemonButt.innerHTML = "Caught";
            pokemonButt.classList.add(styles.caught)
            catchPokemon(pokemon);
        });
    }
    
    pokemonCard.appendChild(pokemonButt);
    pokemonLI.appendChild(pokemonCard);
    return pokemonLI;
}