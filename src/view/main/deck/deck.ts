import styles from './deck.module.scss'

import { IPokemon } from "@/model/model";
import { PokemonService } from "@/service/service";
import { createPokemon } from "./../pokemonCard/pokemon";
import { createLoading } from "../pokemonCard/loading";


//создать стол для покемонов и разложить первую партию карточек
export async function createPokemonDeck():Promise<HTMLElement>{
    const pokemonDeck:HTMLElement = document.createElement('main');
    pokemonDeck.classList.add(styles.pokemonDeck);
    pokemonDeck.append(...(await createPokemonCards()));
    return pokemonDeck;
}

//удалить покемонов со стола
function removePokemons(pokemonDeck:HTMLElement, removeCount:number):void{
    for(let i = Math.abs(removeCount); i >= 1; i--){
        removeCount > 0 ? pokemonDeck.firstChild.remove() : pokemonDeck.lastChild.remove();
    }
}

//создать новые карточки на столе
async function createPokemonCards(up:boolean=false):Promise<HTMLElement[]>{
    const pokemonService = PokemonService.getInstance();
    const pokemons:IPokemon[] = await pokemonService.getPokemons(up);
    const pokemonElements:HTMLElement[] = pokemons.map((pokemon:IPokemon)=>{return createPokemon(pokemon)})
    return pokemonElements
}

//добавить снизу
export async function loadPokemonsDown(pokemonDeck:HTMLElement):Promise<void>{
    const loader:HTMLLIElement = createLoading();
    pokemonDeck.append(loader);
    const pokemonCards:HTMLElement[] = await createPokemonCards();

    if(pokemonCards){
        pokemonDeck.append(...pokemonCards);
        loader.remove();
        let offset:number = PokemonService.getInstance().getOffset();
        if(pokemonDeck.childElementCount > offset * 3){
        removePokemons(pokemonDeck, offset)
        }
    }
}

//добавить вверху
export async function loadPokemonsUp(pokemonDeck:HTMLElement):Promise<void> {
    const loader:HTMLLIElement = createLoading();
    pokemonDeck.prepend(loader);
    const pokemonCards:HTMLElement[] = await createPokemonCards(true);

    if(pokemonCards){
        pokemonDeck.prepend(...pokemonCards);
        loader.remove();
        let offset:number = PokemonService.getInstance().getOffset() * 2;
        if(pokemonDeck.childElementCount > offset){
            removePokemons(pokemonDeck, -(pokemonDeck.childElementCount - offset));
        }
    }
}


