// Add all objects with data into this directory
import { IPokemon } from "@/model/model";

export class Pokemon implements IPokemon{
    public id: number;
    public name: string;
    public imgUrl: string;
    public isCatched: boolean;

    public constructor(name:string, id:number, imgUrl:string){
        this.id = id;
        this.name = name;
        this.imgUrl = imgUrl;
        this.isCatched = false;
    }

    public catch():void {
        this.isCatched=true;
    }
}

export const Pokemons:Array<Pokemon> = [];