// Add all interfaces into this directory

export interface IUrl{
    protocol: string;
    hostName: string;
    version?: string;
    route?: string;
    name?:string;
    args?:string
    getUrl: Function;
    setArgs:Function;
}

export interface IPokemonTransfer{
    getPokemons(url:string):Promise<ValidatePokemons>;
    getPokemon(offset:string):Promise<ValidatePokemonData>;
}

export interface IPokemonService{
    isFetching:boolean;
    getOffset():number ;
    firstLoadPokemons():Promise<void>;
    getPokemons(up:boolean):Promise<IPokemon[]>;
}

export interface IPokemon{
    id: number;
    name: string;
    imgUrl: string;
    isCatched: boolean;
    catch():void;
}

export interface ValidatePokemons{
    count:number;
    next:string;
    results:Array<ValidatePokemon|IPokemon>
    [key:string]:any;
}

export interface ValidatePokemon{
    name:string;
    url:string;
}

export interface ValidatePokemonData{
    id:number;
    name:string;
    sprites:ValidatePokemonImg;
    [key:string]:any;
}

export interface ValidatePokemonImg{
    front_default:string;
    [key:string]:any;
}
