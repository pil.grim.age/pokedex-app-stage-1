// Add all transport classes with api requests into this directory
import {IUrl, IPokemonTransfer, ValidatePokemons, ValidatePokemonData} from "@/model/model"


export class PokemonTransfer implements IPokemonTransfer{
    private static instance:PokemonTransfer;
    private apiUrl:IUrl;
    
    private constructor(){
        this.apiUrl = {
            protocol: 'https',
            hostName: 'pokeapi.co',
            version: '/api/v2/',
            route: 'pokemon/',
            args: '',
            getUrl: ():string => {
                return this.apiUrl.protocol + '://' + this.apiUrl.hostName + this.apiUrl.version + this.apiUrl.route;
            },
            setArgs: (args?: { [key: string]: string | number }):void =>{
                const params:string = Object.keys(args)
                                            .map(key => args[key] ? `${key}=${args[key]}` : '')
                                            .join('&');
                const restUrl:string = args.length ? ('?' + params) : '';
                this.apiUrl.args = restUrl;
            }
        }
    }

    public static getInstance(): PokemonTransfer {
        if (!PokemonTransfer.instance) {
            PokemonTransfer.instance = new PokemonTransfer();
        }
        return PokemonTransfer.instance;
    }

    public async getPokemons(url?:string):Promise<ValidatePokemons>{
        try {
            const response = await fetch(url ? url : this.apiUrl.getUrl());
            return await response.json();
        } catch (error) {
            console.error(error);
        }
    }

    public async getPokemon(name:string):Promise<ValidatePokemonData>{
        try {
            const url = this.apiUrl.getUrl() + name;
            const response = await fetch(url);
            return await response.json();
        } catch (error) {
            console.error(error);
        }
    } 
}