// Add all services into this directory

import { IPokemonService, ValidatePokemonData, ValidatePokemons} from "@/model/model";
import { PokemonTransfer } from "@/transport/transport";
import { Pokemons, Pokemon } from "@/store/store";

export function catchPokemon(pokemon:Pokemon):void{
    pokemon.catch();
    console.log(`${pokemon.name} has been cought! His id is ${pokemon.id}`)
}

export class PokemonService implements IPokemonService{
    private static instance:PokemonService;
    
    private pokemonTransfer:PokemonTransfer;
    private next:string;
    
    private offset:number;
    private currentPage:number;
    private lastLoadedPage:number;
    private totalPage:number;

    public isFetching:boolean;

    private constructor(){
        this.pokemonTransfer = PokemonTransfer.getInstance();
        this.offset = 20;
        this.currentPage = -1;
        this.lastLoadedPage = 0;
        this.isFetching = false;      
    }

    public static getInstance():PokemonService {
        if (!PokemonService.instance) {
            PokemonService.instance = new PokemonService();
        }
        return PokemonService.instance;
    }

    public getOffset():number {
        return this.offset;
    }

    //загрузка первой пачки, для определения необходимых полей
    public async firstLoadPokemons():Promise<void>{
        const pokemons:ValidatePokemons = await this.loadPokemons();
        this.totalPage = pokemons.count/this.offset;
        this.next = pokemons.next;
        this.offset = pokemons.results.length;
    }

    //скачивание пачки покемонов
    private async loadPokemons(url?:string):Promise<ValidatePokemons>{
        const pokemons:ValidatePokemons = await this.pokemonTransfer.getPokemons(url);
        
        let formatPokemons:Pokemon[] = await Promise.all(pokemons.results.map(async pokemon => {
            try{
                const pokemonData:ValidatePokemonData = await this.pokemonTransfer.getPokemon(pokemon.name);
                return new Pokemon(this.formatName(pokemonData.name), pokemonData.id, pokemonData.sprites.front_default);
            } catch(error){
                return new Pokemon('Error', error.code, "");
            }
        }))

        Pokemons.push(...formatPokemons);
        this.lastLoadedPage++;
        return pokemons;
    }

    //получение пачки покемонов для  отрисовки их на карточках. если их нет в сторе, то подгружаем с апи
    public async getPokemons(up:boolean=false):Promise<Pokemon[]>{
        
        if(up && this.currentPage <= 1 || !up && this.currentPage >= this.totalPage){
            return [];
        }

        let currentOffset = (up ? (--this.currentPage - 1) : (++this.currentPage)) * this.offset;

        if(this.next && this.currentPage >= this.lastLoadedPage){
            this.next = (await this.loadPokemons(this.next)).next;
        }
        
        return Pokemons.slice(currentOffset, currentOffset+this.offset);
    }

    private formatName(name: string):string {
        return name
            .split('-')
            .map(word => word.charAt(0).toUpperCase() + word.slice(1).toLowerCase())
            .join(' ');
    }
}